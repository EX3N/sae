#define SAEFUNC __attribute__((visibility("default")))
#include <sae/sae.h>

SAEFUNC int init(SAE_Module*);
SAEFUNC int quit();
SAEFUNC int test();

SAEFUNC int start();
SAEFUNC int stop();

