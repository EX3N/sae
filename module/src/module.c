#include "module.h"
#include "../../sae-event/src/event.h"
#include "../../sae-log/src/log.h"
#include <sae/sae.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

static SAE_Module *g_Mod;

LogID (*SetupLogging)(SAE_LogLevel, const char *);
void (*SetLogLevel)(LogID, SAE_LogLevel);
void (*Log)(LogID, SAE_LogLevel, const char *);

SAE_EventType (*event_type)(const char *);
int (*event_register)(SAE_Event *);

static LogID logger;

int init(SAE_Module *module) {
  g_Mod = module;
  {
    SAE_Function *temp;

    temp = SAE_ModuleFunction(g_Mod, "sae-log", "SetupLogging");
    if (temp == NULL || temp->loaded == 0) {
      return -1;
    } else {
      SetupLogging = temp->func;
    };

    temp = SAE_ModuleFunction(g_Mod, "sae-log", "SetLogLevel");
    if (temp == NULL || temp->loaded == 0) {
      return -1;
    } else {
      SetLogLevel = temp->func;
    };

    temp = SAE_ModuleFunction(g_Mod, "sae-log", "Log");
    if (temp == NULL || temp->loaded == 0) {
      return -1;
    } else {
      Log = temp->func;
    };

    temp = SAE_ModuleFunction(g_Mod, "sae-event", "event_type");
    if (temp == NULL || temp->loaded == 0)
      return -1;
    else
      event_type = temp->func;

    temp = SAE_ModuleFunction(g_Mod, "sae-event", "event_register");
    if (temp == NULL || temp->loaded == 0)
      return -1;
    else
      event_register = temp->func;
  }

  logger = SetupLogging(LOG_INFO, "module");
  return 0;
}

int quit() { return 0; }

typedef struct {
  Display *dpy;
  Window root;
  XEvent ev;
  unsigned int modifiers;
  int keycode;
  Window grab_window;
  Bool owner_events;
  int pointer_mode;
  int keyboard_mode;
} CATCH;

static CATCH catcher = {0};

uint8_t check(void *data) {
  CATCH *c = &catcher;
  XNextEvent(c->dpy, &c->ev);
  return c->ev.type == KeyPress;
}

size_t x = 0;
uint8_t run(void *data) {
  LOG(logger, LOG_WARN, "Got event no. %8lu", x);
  x += 1;
  return 0;
}

static SAE_Event e = {
    .type = 0,
    .rundata = NULL,
    .run = run,
    .checkdata = &catcher,
    .check = check,
};

int start() {
  catcher.dpy = XOpenDisplay(0);
  catcher.root = DefaultRootWindow(catcher.dpy);
  catcher.modifiers = 0;
  catcher.keycode = XKeysymToKeycode(catcher.dpy, XK_Y);
  catcher.grab_window = catcher.root;
  catcher.owner_events = False;
  catcher.pointer_mode = GrabModeAsync;
  catcher.keyboard_mode = GrabModeAsync;

  XGrabKey(catcher.dpy, catcher.keycode, catcher.modifiers, catcher.grab_window,
           catcher.owner_events, catcher.pointer_mode, catcher.keyboard_mode);

  XSelectInput(catcher.dpy, catcher.root, KeyPressMask);
  XGrabKey(catcher.dpy, catcher.keycode, catcher.modifiers, catcher.grab_window,
           catcher.owner_events, catcher.pointer_mode, catcher.keyboard_mode);

  XSelectInput(catcher.dpy, catcher.root, KeyPressMask);
  Log(logger, LOG_WARN, "Grabbed Y!!");

  event_register(&e);
  return 0;
}
int stop() {
  XCloseDisplay(catcher.dpy);
  return 0;
}
