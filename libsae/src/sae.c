#include "sae.h"
#include <stdio.h>
#include <string.h>

SAE_Function *SAE_GetFunction(SAE_Module *module, const char *fn) {
  size_t i = 0;
  for (i = 0; i < module->fnamount + 4; i++) {
    if (strcmp(fn, module->funcs[i]->name) == 0) {
      return module->funcs[i];
    }
  }
  return NULL;
}

SAE_Function *SAE_ModuleFunction(SAE_Module *module, const char *modulename,
                                 const char *fn) {
  size_t i = 0, j = 0;
  for (i = 0; i < module->importamount; i++) {
    if (strcmp(modulename, module->imports[i]->name) == 0) {
      const SAE_Imports *import = module->imports[i];
      for (j = 0; j < import->fnamount; j++) {
        if (strcmp(fn, import->functions[j]->name) == 0) {
          return import->functions[j];
        }
      }
    }
  }
  return NULL;
}
int SAE_InitializeModule(SAE_Module **module) {
  typedef int (*init_t)(SAE_Module *);
  const init_t fn = SAE_GetFunction(*module, "init")->func;
  return (fn)(*module);
}

int SAE_StartModule(SAE_Module *module) {
  typedef int (*start_t)();
  const start_t fn = SAE_GetFunction(module, "start")->func;
  return (fn)(module);
}

int SAE_StopModule(SAE_Module *module) {
  typedef int (*stop_t)();
  const stop_t fn = SAE_GetFunction(module, "stop")->func;
  return (fn)(module);
}

int SAE_QuitModule(SAE_Module *module) {
  typedef int (*quit_t)();
  const quit_t fn = SAE_GetFunction(module, "quit")->func;
  return (fn)(module);
}
