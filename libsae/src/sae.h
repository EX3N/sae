#ifndef INCLUDE_SAE_SAE_H
#define INCLUDE_SAE_SAE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

#if defined(_WIN32) || defined(_WIN64)
#define SAE_WINDOWS
#elif defined(__linux__) && (!defined(__ANDROID__))
#define SAE_LINUX
#define SAE_UNIX
#else
#error "OS Not supported by sae"
#endif

#if defined(__unix__) || defined(__unix) || defined(unix)
#define SAE_UNIX
#endif

#ifdef SAE_WINDOWS
#include <Windows.h>
typedef HMODULE SAE_Library_t;
typedef FARPROC SAE_Func_t;
#else
typedef void *SAE_Library_t;
typedef void *SAE_Func_t;
#endif

typedef uint64_t SAE_Version;

#define SAE_VERSION(MAJOR, MINOR, PATCH, BUILD)                                \
  ((uint64_t)((uint16_t)MAJOR)) << 48 | ((uint64_t)((uint16_t)MINOR)) << 32 |  \
      ((uint64_t)((uint16_t)PATCH)) << 16 | ((uint64_t)((uint16_t)BUILD))

struct SAE_Module;

typedef uint8_t (*SAE_Init)(struct SAE_Module *);
typedef uint8_t (*SAE_Exit)();

typedef uint8_t (*SAE_Start)();
typedef uint8_t (*SAE_Stop)();

typedef enum SAE_FuncType {
  SAE_FUNC_INIT,
  SAE_FUNC_QUIT,
  SAE_FUNC_START,
  SAE_FUNC_STOP,
  SAE_FUNC_CUSTOM,
} SAE_FuncType;

typedef struct {
  SAE_FuncType type;
  uint8_t loaded;
  const char *name;
  SAE_Func_t func;
} SAE_Function;

typedef struct {
  const char *name;
  uint8_t loaded;
  size_t fnamount;
  SAE_Function **functions;
} SAE_Imports;

typedef struct SAE_Module {
  char *name;
  char *desc;
  char *author;
  char *license;

  SAE_Version version;

  char *path;
  SAE_Library_t module;

  uint8_t loaded;

  SAE_Function **funcs;
  size_t fnamount;

  SAE_Imports **imports;
  size_t importamount;
} SAE_Module;

int SAE_InitializeModule(SAE_Module **module);
int SAE_StartModule(SAE_Module *module);
int SAE_StopModule(SAE_Module *module);
int SAE_QuitModule(SAE_Module *module);
SAE_Function *SAE_GetFunction(SAE_Module *module, const char *fn);
SAE_Function *SAE_ModuleFunction(SAE_Module *module, const char *modulename,
                                 const char *fn);

#ifdef __cplusplus
}
#endif

#endif
