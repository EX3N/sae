#include "module.h"
#include <sae/sae.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../../sae-log/src/log.h"

static SAE_Module *g_Mod;

LogID (*SetupLogging)(SAE_LogLevel, const char *);
void (*SetLogLevel)(LogID, SAE_LogLevel);
void (*Log)(LogID, SAE_LogLevel, const char *);

static LogID logger;

int init(SAE_Module *module) {
  g_Mod = module;
  {
    SAE_Function *temp;

    temp = SAE_ModuleFunction(g_Mod, "sae-log", "SetupLogging");
    if (temp == NULL || temp->loaded == 0) {
      return -1;
    } else {
      SetupLogging = temp->func;
    };

    temp = SAE_ModuleFunction(g_Mod, "sae-log", "SetLogLevel");
    if (temp == NULL || temp->loaded == 0) {
      return -1;
    } else {
      SetLogLevel = temp->func;
    };

    temp = SAE_ModuleFunction(g_Mod, "sae-log", "Log");
    if (temp == NULL || temp->loaded == 0) {
      return -1;
    } else {
      Log = temp->func;
    };
  }

  logger = SetupLogging(LOG_INFO, "module");
  printf("fasdfas\n");
  return 0;
}

int quit() { return 0; }

int start() {
  Log(logger, LOG_WARN, "Starting networking!!");
  return 0;
}
int stop() { return 0; }

int CreateDevice(SAE_NetDevice *device, SAE_NetDeviceType type) {
  if (device == NULL)
    return -1;

  device->type = type;

  switch (device->type) {
  case SAE_Slave:
    break;
  case SAE_Master:
    break;
  }

  return 0;
}
int ConnectToSlave(SAE_NetDevice* device, const char* ip) {
  return 0;
}
