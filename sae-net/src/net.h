#ifndef INCLUDE_NET_H
#define INCLUDE_NET_H

#include <stdint.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

typedef enum {
  SAE_Slave,
  SAE_Master,
} SAE_NetDeviceType;

typedef uint64_t SAE_NetDeviceID;
struct SAE_NetDevice;

typedef struct SAE_NetDevice {
  SAE_NetDeviceType type;
  SAE_NetDeviceID id;

  struct in_addr address;

  struct in_addr master_address;

  struct SAE_NetDevice *slaves;
  size_t slaveamount;
} SAE_NetDevice;


#endif
