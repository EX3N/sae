#define SAEFUNC __attribute__((visibility("default")))
#include <sae/sae.h>
#include "net.h"

SAEFUNC int init(SAE_Module*);
SAEFUNC int quit();

SAEFUNC int start();
SAEFUNC int stop();

SAEFUNC int CreateDevice(SAE_NetDevice*,SAE_NetDeviceType);
SAEFUNC int ConnectToSlave(SAE_NetDevice*, const char*);

