#!/bin/sh

git rev-parse --show-toplevel | xargs -I% cd "%"

if [ -n "$1" ]; then
  dbg="DEBUG=1"
else
  dbg=""
fi

compresscmd="7z a"

cd libsae
mkdir .obj
make clean && make $dbg && sudo make install
cd ..

cd sae
mkdir .obj
mkdir .obj/linux
make clean && make $dbg
cd ..

build() {
  cd $1
  mkdir .obj
  make clean && make $dbg && make package # TODO fix compression COMPRESS="$compresscmd"
  cd ..
}

for VAR in ./sae-*/ 
do
  build $VAR
done

echo done
