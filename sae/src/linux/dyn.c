#include <sae/sae.h>
#include <dlfcn.h>
#include <stdio.h>

#include "dyn.h"

SAE_Library_t load_lib(const char *path) {
  void *lib = dlopen(path, RTLD_LAZY);
  if (!lib) {
    fprintf(stderr, "Failed to %s. Error: %s", path, dlerror());
  }
  return lib;
}

void close_lib(SAE_Library_t lib) {
  dlclose(lib);
}

SAE_Func_t load_func(SAE_Library_t *lib, const char *fn) {
  void *fun = dlsym(lib, fn);
  const char *dlsym_error = dlerror();
  if (dlsym_error) {
    fprintf(stderr, "failed to load symbol %s\n", dlsym_error);
  }

  return fun;
}
