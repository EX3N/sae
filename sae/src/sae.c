#include <archive.h>
#include <archive_entry.h>
#include <sae/sae.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dyn.h"
#include "ini.h"

#define BLOCKSIZE 1024

uint8_t load(char *path, SAE_Module *module);
uint8_t import(SAE_Module **module, size_t modulec);
uint8_t clean_module(SAE_Module **module);
char *cache(char *path);

int main(int argc, char **argv) {
  if (argc < 2) {
    return -1;
  }

  SAE_Module *modules[argc];

  for (int i = 0; i < argc - 1; i++) {
    modules[i] = malloc(sizeof(SAE_Module));
    if (load(argv[i + 1], modules[i]) != 0) {
      fprintf(stderr, "failed to load %s\n", argv[i + 1]);
      return -1;
    }

    printf("[%02d/%02d] Loaded %s made by %s. Version %lx\n", i + 1, argc - 1,
           modules[i]->name, modules[i]->author, modules[i]->version);
  }
  if (import(modules, argc - 1) != 0) {
    fprintf(stderr, "failed to import funcs\n");
  } else {
    fprintf(stderr, "import succesful\n");
  }
  printf("\n  ================  \n\n");

  for (int i = 0; i < argc - 1; i++) {
    SAE_InitializeModule(&(modules[i]));
  }
  for (int i = 0; i < argc - 1; i++) {
    SAE_StartModule(modules[i]);
  }
  sleep(5);
  for (int i = 0; i < argc - 1; i++) {
    SAE_StopModule(modules[i]);
  }
  for (int i = 0; i < argc - 1; i++) {
    SAE_QuitModule(modules[i]);
  }
  printf("\n\n  ================  \n\n");

  for (int i = 0; i < argc - 1; i++) {
    clean_module(modules + i);
  }

  return 0;
}

uint8_t import(SAE_Module **modules, size_t modulec) {
  size_t i = 0;
  for (i = 0; i < modulec; i++) {
    if (modules[i]->importamount == 0) {
      continue;
    }
    size_t j = 0;
    for (j = 0; j < modules[i]->importamount; j++) {
      SAE_Imports *imports = modules[i]->imports[j];

      size_t found = 0;
      size_t mindex = 0; /* module index */
      size_t k = 0;
      for (k = 0; k < modulec; k++) {
        if (strcmp(imports->name, modules[k]->name) == 0) {
          found = 1;
          mindex = k;
          break;
        }
      }

      if (found == 0) {
        fprintf(stderr, "Module %s NOT FOUND!!\n", imports->name);
        continue;
      }

      SAE_Module *immodule = modules[mindex];

      imports->loaded = 1;
      for (k = 0; k < imports->fnamount; k++) {
        SAE_Function *fn = imports->functions[k];
        found = 0;
        size_t l = 0;
        for (l = 0; l < immodule->fnamount + 4; l++) {
          if (strcmp(fn->name, immodule->funcs[l]->name) == 0) {
            if (immodule->funcs[l]->loaded && immodule->funcs[l]->func != 0) {
              fn->func = immodule->funcs[l]->func;
              fn->loaded = 1;
              found = 1;
              break;
            }
          }
        }
        imports->loaded &= found;
        if (found == 0) {
          fprintf(stderr, "failed to find function %s for %s from %s\n",
                  fn->name, modules[i]->name, immodule->name);
        }
      }
      if (imports->loaded) {
        fprintf(stderr, "succcesfully loaded all functions for %s from %s\n",
                modules[i]->name, immodule->name);
      }
    }
  }
  return 0;
}

uint8_t clean_module(SAE_Module **module) {
  size_t i = 0;
  size_t j = 0;
  SAE_Module *mod = *module;

  printf("freeing %s\n", (*module)->name);
  free(mod->name);
  free(mod->path);
  close_lib(mod->module);
  return -1;

  for (i = 0; i < mod->fnamount + 4; i++) {
    free((void *)mod->funcs[i]->name);
    free(mod->funcs[i]);
  }

  for (i = 0; i < mod->importamount; i++) {
    for (j = 0; j < mod->imports[i]->fnamount; j++) {
      free((void *)mod->imports[i]->functions[j]->name);
      free(mod->imports[i]->functions[j]);
    }
    free(mod->imports[i]);
  }
  free(mod->imports);

  free(*module);
  return 0;
}

static char *clean_str(const char *value) {
  char *val = NULL;
  if (value[0] != '"' || value[strlen(value) - 1] != '"') {
    fprintf(stderr, "failed parsing\n");
    return NULL;
  }
  size_t l = 0;
  size_t i = 0;
  for (i = 1; i < strlen(value) - 1; i++) {
    if (value[i] != '\n' && value[i] != ' ') {
      l++;
    }
  }

  val = malloc(sizeof(char) * (l + 1));
  i = 1;
  size_t j = 0;
  while (i < strlen(value) - 1) {
    if (value[i] != '\n' && value[i] != ' ') {
      val[j] = value[i];
      j++;
    }
    i++;
    val[l] = 0;
  }
  return val;
}

static int handler(void *user, const char *section, const char *name,
                   const char *value) {
  SAE_Module *conf = *((SAE_Module **)(user));

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
  if (MATCH("module", "name")) {
    conf->name = clean_str(value);
  } else if (MATCH("module", "version")) {
    char *val = clean_str(value);
    uint16_t vers[4];
    size_t i = 0;

    char *ptr[] = {val, val, val, val, val};
    const int base = 10;
    for (i = 0; i < 4; i++) {
      vers[i] = strtol(ptr[i], ptr + i + 1, base);
      ptr[i + 1]++;
    }
    conf->version = SAE_VERSION(vers[0], vers[1], vers[2], vers[3]);
  } else if (MATCH("module", "author")) {
    conf->author = clean_str(value);
  } else if (MATCH("module", "description")) {
    conf->desc = clean_str(value);
  } else if (MATCH("module", "license")) {
    conf->license = clean_str(value);
  } else if (MATCH("module", "export")) {
    char *val = clean_str(value);
    size_t len = strlen(val);
    size_t i = 0;

    conf->fnamount = 0;
    for (i = 0; i < len; i++) {
      conf->fnamount += val[i] == ',';
    }
    if (conf->fnamount > 0) {
      conf->fnamount++;
    }

    conf->funcs =
        (SAE_Function **)malloc(sizeof(SAE_Function *) * (4 + conf->fnamount));

    conf->funcs[0] = (SAE_Function *)malloc(sizeof(SAE_Function));
    conf->funcs[0]->type = SAE_FUNC_INIT;
    conf->funcs[0]->name = "init";

    conf->funcs[1] = (SAE_Function *)malloc(sizeof(SAE_Function));
    conf->funcs[1]->type = SAE_FUNC_QUIT;
    conf->funcs[1]->name = "quit";

    conf->funcs[2] = (SAE_Function *)malloc(sizeof(SAE_Function));
    conf->funcs[2]->type = SAE_FUNC_START;
    conf->funcs[2]->name = "start";

    conf->funcs[3] = (SAE_Function *)malloc(sizeof(SAE_Function));
    conf->funcs[3]->type = SAE_FUNC_STOP;
    conf->funcs[3]->name = "stop";

    size_t s = 0;
    size_t n = 4;
    for (i = 0; i < len; i++) {
      if (val[i] == ',' || i == len - 1) {
        conf->funcs[n] = (SAE_Function *)malloc(sizeof(SAE_Function));
        conf->funcs[n]->name =
            strndup(val + s, i == len - 1 ? i - s + 1 : i - s);
        conf->funcs[n]->type = SAE_FUNC_CUSTOM;
        s = ++i;
        n++;
      }
    }
  } else if (MATCH("module", "import")) {
    char *val = clean_str(value);

    size_t i = 0;
    size_t index = 0;
    size_t len = strlen(val);
    size_t *splits = NULL;

    for (i = 0; i < len; i++) {
      if (val[i] == ';') {
        conf->importamount++;
      }
    }
    if (len != 0) {
      conf->importamount++;
    } else {
      return 0;
    }

    conf->imports =
        (SAE_Imports **)malloc(sizeof(SAE_Imports *) * conf->importamount);

    splits = (size_t *)malloc(sizeof(size_t) * (conf->importamount + 2));
    splits[0] = 0;
    splits[conf->importamount] = len;
    for (i = 0; i < len; i++) {
      if (val[i] == ';') {
        splits[index + 1] = i + 1;
        index++;
      };
    }

    for (index = 0; index < conf->importamount; index++) {
      conf->imports[index] = (SAE_Imports *)malloc(sizeof(SAE_Imports));
      memset(conf->imports[index], 0, sizeof(SAE_Imports));
      size_t c = 0;
      for (i = splits[index]; i < splits[index + 1] - 1; i++)
        if (val[i] == ':') c = i;
      conf->imports[index]->name =
          strndup(val + splits[index], c - splits[index]);

      for (i = c; i < splits[index + 1] - 1; i++)
        if (val[i] == ',') conf->imports[index]->fnamount++;
      if (conf->imports[index]->fnamount != 0)
        conf->imports[index]->fnamount++;
      else
        return 0;

      conf->imports[index]->functions = (SAE_Function **)malloc(
          sizeof(SAE_Function *) * conf->imports[index]->fnamount);
      size_t s = c + 1, jindex = 0;
      for (i = s; i < splits[index + 1] - 1; i++) {
        const size_t offset = index == conf->importamount - 1 ? 2 : 3;
        if (val[i] == ',' || i == splits[index + 1] - offset) {
          conf->imports[index]->functions[jindex] =
              (SAE_Function *)malloc(sizeof(SAE_Function));
          memset(conf->imports[index]->functions[jindex], 0,
                 sizeof(SAE_Function));

          conf->imports[index]->functions[jindex]->type = SAE_FUNC_CUSTOM;
          conf->imports[index]->functions[jindex]->name = strndup(
              val + s, i == splits[index + 1] - offset ? i - s + 2 : i - s);

          s = i + 1;
          jindex++;
        }
      }
    }
  } else {
    return 0; /* unknown section/name, error */
  }
  return 1;
}

uint8_t load(char *path, SAE_Module *module) {
  memset(module, 0, sizeof(SAE_Module));
  struct archive *a = NULL;
  struct archive_entry *ae = NULL;
  int r = 0;

  a = archive_read_new();
  archive_read_support_format_all(a);
  archive_read_support_filter_all(a);

  if ((r = archive_read_open_filename(a, path, BLOCKSIZE))) {
    fprintf(stderr, "failed to open %s", path);
    return 1;
  }

  char *extract_path = NULL;
  {
    size_t i = 0;
    size_t last_slash = 0;
    for (i = strlen(path) - 1; i >= 1; i--) {
      if (path[i] == '/') {
        last_slash = i;
        break;
      }
    }
    extract_path = cache(path + last_slash + 1);

    const size_t perm = 0777;
    mkdir(cache(""), perm);
    mkdir(extract_path, perm);
  }

  for (;;) {
    r = archive_read_next_header(a, &ae);
    if (r == ARCHIVE_EOF) {
      break;
    }
    if (r != ARCHIVE_OK) {
      fprintf(stderr, "archive's not ok :(\n");
    }

    const char *filename = archive_entry_pathname(ae);
    char *name =
        malloc(sizeof(char) * (strlen(filename) + strlen(extract_path) + 2));
    strcpy(name, extract_path);
    name[strlen(extract_path)] = '/';
    strcpy(name + (strlen(extract_path) + 1), filename);

    FILE *fp = fopen(name, "wb");
    if (fp == NULL) {
      printf("FUCKFUCKFUCKFUCK %s IS FUCKED\n", name);
    }
    char *buf = NULL;
    size_t size = 0;
    int64_t offset = 0;
    r = archive_read_data_block(a, (const void **)&(buf), &size, &offset);

    fwrite(buf, sizeof(char), size, fp);
    fclose(fp);

    if (strcmp(filename, "module.ini") == 0) {
      ini_parse_string(buf, handler, &module);
      if (module->funcs == 0) {
        module->funcs = (SAE_Function **)malloc(sizeof(SAE_Function *) * 4);

        module->funcs[0] = (SAE_Function *)malloc(sizeof(SAE_Function));
        module->funcs[0]->type = SAE_FUNC_INIT;
        module->funcs[0]->name = "init";

        module->funcs[1] = (SAE_Function *)malloc(sizeof(SAE_Function));
        module->funcs[1]->type = SAE_FUNC_QUIT;
        module->funcs[1]->name = "quit";

        module->funcs[2] = (SAE_Function *)malloc(sizeof(SAE_Function));
        module->funcs[2]->type = SAE_FUNC_START;
        module->funcs[2]->name = "start";

        module->funcs[3] = (SAE_Function *)malloc(sizeof(SAE_Function));
        module->funcs[3]->type = SAE_FUNC_STOP;
        module->funcs[3]->name = "stop";
      }
    }
    if (strcmp(filename, "module.so") == 0) {
      module->path = name;
    }
  }
  free(extract_path);

  module->module = load_lib(module->path);

  for (size_t i = 0; i < module->fnamount + 4; i++) {
    module->funcs[i]->func = load_func(module->module, module->funcs[i]->name);
    module->funcs[i]->loaded = module->funcs[i]->func != 0;
  }
  /*
  module->init = module->funcs[0]->func;
  module->quit = module->funcs[1]->func;
  module->start = module->funcs[2]->func;
  module->stop = module->funcs[3]->func;
  */

  return 0;
}

char *cache(char *path) {
  char *base = getenv("XDG_CACHE_HOME");
  char *res = malloc(sizeof(char) *
                     (strlen(base) + strlen("/sae/") + strlen(path) + 1));
  strcpy(res, base);
  strcpy(res + strlen(base), "/sae/");
  strcpy(res + strlen(base) + strlen("/sae/"), path);
  return res;
}
