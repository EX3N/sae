#ifndef INCLUDE_DYN_H
#define INCLUDE_DYN_H

#include <sae/sae.h>

SAE_Library_t load_lib(const char *path);
void close_lib(SAE_Library_t lib);
SAE_Func_t load_func(SAE_Library_t *lib, const char *fn);

#endif
