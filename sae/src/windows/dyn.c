#include <sae.h>
#ifdef SAE_WINDOWS
#include <Windows.h>
#include <stdio.h>

#include "dyn.h"

SAE_Library_t load_lib(const char *path) {
  HMODULE *lib = LoadLibrary(path, RTLD_LAZY);
  if (lib == NULL) {
    fprintf(stderr, "Failed to %s", path);
  }
  return lib;
}
SAE_Func_t load_func(SAE_Library_t *lib, const char *fn) {
  FARPROC *fun = GetProcAddress(lib, fn);
  if (fun == NULL) {
    fprintf(stderr, "failed to load symbol %s\n");
  }

  return fun;
}
#endif
