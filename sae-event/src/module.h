#include <sae/sae.h>
#include "event.h"
#define SAEFUNC __attribute__((visibility("default")))

SAEFUNC int init(SAE_Module*);
SAEFUNC int quit();

SAEFUNC int start();
SAEFUNC int stop();

SAEFUNC SAE_EventType event_type(const char *);

SAEFUNC int event_register(SAE_Event*);
