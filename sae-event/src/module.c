#include "module.h"
#include "../../sae-log/src/log.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

static SAE_Module *g_Mod;

LogID (*SetupLogging)(SAE_LogLevel, const char *);
void (*SetLogLevel)(LogID, SAE_LogLevel);
void (*Log)(LogID, SAE_LogLevel, const char *);

static LogID logger = 0xFFFF;

static SAE_Event **events = NULL;
static size_t eventamount = 0;

int init(SAE_Module *module) {
  g_Mod = module;
  {
    SAE_Function *temp;

    temp = SAE_ModuleFunction(g_Mod, "sae-log", "SetupLogging");
    if (temp == NULL || temp->loaded == 0) {
      return -1;
    } else {
      SetupLogging = temp->func;
    };

    temp = SAE_ModuleFunction(g_Mod, "sae-log", "SetLogLevel");
    if (temp == NULL || temp->loaded == 0) {
      return -1;
    } else {
      SetLogLevel = temp->func;
    };

    temp = SAE_ModuleFunction(g_Mod, "sae-log", "Log");
    if (temp == NULL || temp->loaded == 0) {
      return -1;
    } else {
      Log = temp->func;
    };
  }

  return 0;
}

void *event_loop(void *);
static pthread_t thread;
int start() {
  logger = SetupLogging(LOG_INFO, "sae-event");
  event_loop(NULL);
  int r = pthread_create(&thread, NULL, event_loop, NULL);
  if (r != 0) {
    LOG(logger, LOG_ERROR, "Failed to create thread" );
  }

  return 0;
}

static stopped = 0;
int stop() { stopped = 1; return 0; }
int quit() { return 0; }

SAE_EventType event_type(const char *name) { return Hash(name); }

int event_register(SAE_Event *e) {
  if (eventamount == 0) {
    events = malloc(sizeof(SAE_Event*) * 1024);
  }
  events[eventamount++] = e;

  return 0;
}

void *event_loop(void *args) {
  for (;;) {
    for (size_t i = 0; i < eventamount; i++) {
      SAE_Event *event = events[i];
      if (event->check(event->checkdata)) {
        event->run(event->rundata);
      }
    }
    if (stopped)
      pthread_exit(0);
  }
}
