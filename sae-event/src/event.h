#ifndef INCLUDE_EVENT_H
#define INCLUDE_EVENT_H
#include <sae/sae.h>

typedef uint64_t SAE_EventType;
typedef uint8_t (*SAE_EventCheck)(void*);
typedef uint8_t (*SAE_EventRun)(void *);

inline static uint64_t Hash(const char *str) {
  uint64_t hash = 5381;
  char c;
  while ((c = *str++))
    hash = ((hash << 5) + hash) + c;
  return hash;
}

typedef struct {
  SAE_EventType type;
  SAE_EventCheck check;
  void *checkdata;
  SAE_EventRun run;
  void *rundata;

} SAE_Event;

#endif
