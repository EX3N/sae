#ifndef INCLUDE_MODULE_H
#define INCLUDE_MODULE_H

#define SAEFUNC __attribute__((visibility("default")))
#include <sae/sae.h>
#include "log.h"

SAEFUNC int init(SAE_Module*);
SAEFUNC int quit();
SAEFUNC int test();

SAEFUNC int start();
SAEFUNC int stop();

SAEFUNC int stop();

SAEFUNC LogID SetupLogging(SAE_LogLevel,const char*);
SAEFUNC void SetLogLevel(LogID,SAE_LogLevel);
SAEFUNC void Log(LogID,SAE_LogLevel,const char*);

#endif
