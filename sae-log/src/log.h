#ifndef INCLUDE_LOG_H
#define INCLUDE_LOG_H
#include <sae/sae.h>

#define SAEFUNC __attribute__((visibility("default")))

typedef enum {
  LOG_DEBUG = 0,
  LOG_INFO = 1,
  LOG_WARN = 2,
  LOG_ERROR = 3,
  LOG_FATAL = 4,
} SAE_LogLevel;

typedef uint64_t LogID;

typedef struct {
  char *name;
  SAE_LogLevel level;
} LogObject;

#define LOG(logger, level, ...)                                                \
  {                                                                            \
    size_t s = snprintf(NULL, 0, __VA_ARGS__);                                 \
    char *temp = (char *)malloc(s + 2);                                        \
    snprintf(temp, s + 1, __VA_ARGS__);                                        \
    Log(logger, level, temp);                                                  \
    free(temp);                                                                \
  }

#endif
