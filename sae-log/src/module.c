#include "module.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static SAE_Module *g_Mod;
static FILE *g_LogFile;

LogObject *g_Objs;
size_t g_ObjsCap = 1;
size_t g_ObjsAmount = 0;

LogID SetupLogging(SAE_LogLevel level, const char *name) {
  if (g_ObjsAmount == 0) {
    g_Objs = malloc(sizeof(LogObject) * g_ObjsCap);
    g_LogFile = fopen("/tmp/sae.log", "w");
  } else if (g_ObjsCap == g_ObjsAmount) {
    g_ObjsCap *= 2;
    g_Objs = realloc(g_Objs, sizeof(LogObject) * g_ObjsCap);
  }

  g_Objs[g_ObjsAmount].name = malloc(sizeof(char) * (strlen(name) + 1));
  memset(g_Objs[g_ObjsAmount].name, 0, sizeof(char) * (strlen(name) + 1));
  strcpy(g_Objs[g_ObjsAmount].name, name);
  g_Objs[g_ObjsAmount].level = level;
  g_ObjsAmount++;

  return g_ObjsAmount - 1;
}

void SetLogLevel(LogID id, SAE_LogLevel level) {
  printf("ID: %lu\n", id);
  return;
  if (id > g_ObjsAmount)
    return;
  g_Objs[id].level = level;
}

void Log(LogID id, SAE_LogLevel level, const char *msg) {
  if (level >= g_Objs[id].level) {
    char *lev;
    char *ansi_start, *msg_start, *ansi_stop = "\e[0m";
    switch (level) {
    case LOG_DEBUG:
      lev = "DEBUG";
      ansi_start = "\e[3;37m";
      msg_start = ansi_start;
      break;
    case LOG_INFO:
      lev = "INFO";
      ansi_start = "\e[0;32m";
      msg_start = ansi_start;
      break;
    case LOG_WARN:
      lev = "WARN";
      ansi_start = "\e[0;93m";
      msg_start = ansi_start;
      break;
    case LOG_ERROR:
      lev = "ERROR";
      ansi_start = "\e[0;91m";
      msg_start = ansi_start;
      break;
    case LOG_FATAL:
      lev = "FATAL";
      ansi_start = "\e[1;91m";
      msg_start = "\e[7;91m";
      break;
    }

#define COLOR_FORMAT_STRING "%s[%12s]%s %s%s%s\n"
#define FORMAT_STRING "[%12s] <%8s> %s\n"
    {
      const size_t size =
          snprintf(NULL, 0, COLOR_FORMAT_STRING, msg_start, g_Objs[id].name,
                   ansi_stop, ansi_start, msg, ansi_stop);
      char *buffer = malloc(size);
      snprintf(buffer, size, COLOR_FORMAT_STRING, msg_start, g_Objs[id].name,
               ansi_stop, ansi_start, msg, ansi_stop);

      fprintf(stdout, "%s\n", buffer);
    }

    {
      const size_t size =
          snprintf(NULL, 0, FORMAT_STRING, g_Objs[id].name, lev, msg);
      char *buffer = malloc(size);
      snprintf(buffer, size, FORMAT_STRING, g_Objs[id].name, lev, msg);
      if (g_LogFile != NULL) {
        fprintf(g_LogFile, "%s\n", buffer);
      }
    }
  }
}

int init(SAE_Module *module) {
  g_Mod = module;
  return 0;
}

int quit() {
  if (g_LogFile == NULL) {
    fclose(g_LogFile);
  }
  return 0;
}

int start() { return 0; }

int stop() {
  // fclose(g_LogFile);
  return 0;
}
